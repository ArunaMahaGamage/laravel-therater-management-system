<html>
    <head>
        <title>App Name - @yield('title')</title>
                <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
     <div class="flex-center position-ref full-height">
     <div class="content">
     <a href="{{ url('/blade') }}"><button type="submit" class="btn btn-default" >Update</button></a>
     <a href="{{url('/actor.show') }}"><button type="button" class="btn btn-default">Retrive</button></a>
     <a href="{{ url('/actor.delete') }}"><button type="button" class="btn btn-default">Delete</button></a>
                <div class="title m-b-md">
                    Movie
                </div>
                <form method='post' action='/movie'>

                <div class="links">
                     <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <div class="form-group">
                              <label for="name_label">Director :</label>
                              <input type="text" class="form-control" id="director"  name="director">
                        </div>
                        <div class="form-group">
                            <label for="bio_label">Title :</label>
                            <input type="text" class="form-control" id="title"  name="title">
                        </div>
                        <div class="form-group">
                            <label for="phpto_label">Release Date :</label>
                            <input type="text" class="form-control" id="release_date"  name="release_date">
                        </div>
                        
                        <button type="submit" class="btn btn-default">Submit</button>                        
                    
                </div>
                </form>
            </div>
     </div>
    </body>
</html>