<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/actor','ActorController');

Route::resource('/movie','MovieController');

Route::resource('/show','ShowController');

Route::resource('/screen','ScreenController');

Route::resource('/theater','TheaterController');

//

//Route::post('/delete','ActorController@destroy');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/blade', function () {
    return view('actor.actorupdate');
});
Route::get('/actor.delete', function () {
    return view('actor.actordistroy');
});

Route::get('/movie.showdata','MovieController@show');

Route::post('/delete/{actor}','ActorController@destroy');

Route::get('/actor.show','ActorController@show');