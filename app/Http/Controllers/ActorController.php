<?php

namespace App\Http\Controllers;

use App\Actor;
use Illuminate\Http\Request;

class ActorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('actor.actor');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        echo 'in Store()<br/>';
        $actor = new Actor;

        $actor->name = $request->name;
        $actor->bio = $request->bio;
        $actor->photo = $request->photo;

        $actor->save();;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Actor  $actor
     * @return \Illuminate\Http\Response
     */
    public function show(Actor $actor)
    {
        //
        $actor = Actor::find($actor->id);

        return view('actor.actorshow',$actor);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Actor  $actor
     * @return \Illuminate\Http\Response
     */
    public function edit(Actor $actor)
    {
        //
        echo 'in edit';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Actor  $actor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Actor $actor)
    {
        //
        echo 'in update';
        $actoredit = Actor::find($request->id);

        $actoredit->name = $request->name;
        $actoredit->age = $request->bio;
        $actoredit->grade = $request->photo;

        $actoredit->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Actor  $actor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Actor $actor)
    {
        //
        echo 'in Destroy';
        $actordestroy = Actor::find($actor->id);
        $actordestroy->delete();
    }

    public function loadupdate() {
        return view('actor.actorupdate');
    }
}
